export const defaults = {
  column: {
    title: "Click to change title"
  },
  card: {
    title: "New Card Title",
    description: "Description",
    labels: ["lorem", "ipsum"]
  }
};

export const initialBoard = {
  "boardList": [
    {
      "id": 0,
      "name": "Welcome (Click to change board name)"
    }
  ],
  "columnList": [
    {
      "id": 0,
      "boardId": 0,
      "title": "Stuff to do"
    },
    {
      "id": 1,
      "boardId": 0,
      "title": "Click to change title"
    }
  ],
  "cardList": {
    "cards": [
      {
        "id": 0,
        "columnId": 0,
        "title": "This is the card",
        "description": "You can edit cards to click pencil icon."
      },
      {
        "id": 1,
        "columnId": 0,
        "title": "Draggable",
        "description": "You can drag and drop cards between lists."
      },
      {
        "id": 2,
        "columnId": 0,
        "title": "Dependencies",
        "description": "Cards can be depends on another cards."
      }
    ],
    "labels": [
      {
        "id": 0,
        "name": "first"
      },
      {
        "id": 1,
        "name": "card"
      },
      {
        "id": 2,
        "name": "drag"
      },
      {
        "id": 3,
        "name": "this"
      },
      {
        "id": 4,
        "name": "has"
      },
      {
        "id": 5,
        "name": "dependency"
      }
    ],
    "cardLabelRelation": [
      [
        0,
        0,
        "#3385ff"
      ],
      [
        0,
        1,
        "#9966ff"
      ],
      [
        1,
        2,
        "#ff6666"
      ],
      [
        1,
        3,
        "#00cc66"
      ],
      [
        1,
        1,
        "#b30086"
      ],
      [
        2,
        4,
        "#ff8533"
      ],
      [
        2,
        5,
        "#00cc66"
      ]
    ],
    "cardDependencies": {
      "0": {
        "cardId": 0,
        "parentList": [],
        "childrenList": [
          2
        ]
      },
      "1": {
        "cardId": 1,
        "parentList": [],
        "childrenList": []
      },
      "2": {
        "cardId": 2,
        "parentList": [
          0
        ],
        "childrenList": []
      }
    }
  }
};