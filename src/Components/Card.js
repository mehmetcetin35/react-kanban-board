import React, { Component } from 'react';
import { connect } from 'react-redux';
import { modalShow, moveCardToColumn, removeCard } from "../Actions";
import CardLabel from '../Components/CardLabel';
import '../Styles/Card.css';
import FaEdit from 'react-icons/lib/fa/pencil';
import FaMove from 'react-icons/lib/fa/arrows';
import FaRemove from 'react-icons/lib/fa/close';

class Card extends Component {
    isClickEvent = false
    constructor(props) {
        super(props);
        this.state = {
            labelInput: ""
        };
        this.onClickShowEditCardModal = this.onClickShowEditCardModal.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onClickRemoveCard = this.onClickRemoveCard.bind(this);
    }

    onClickShowEditCardModal(e) {
        this.props.showEditCardModal({
            cardId: this.props.id
        });
        this.isClickEvent = true;
        const _self = this;
        setTimeout(() => {
            _self.isClickEvent = false;
        }, 10);
    }

    onClickRemoveCard(e) {
        this.props.removeCard();
        this.isClickEvent = true;
        const _self = this;
        setTimeout(() => {
            _self.isClickEvent = false;
        }, 10);
    }

    onMouseDown(e) {
        if (this.isClickEvent === true) return;

        const { ref } = this;
        let cloneElm = ref.cloneNode(true);
        ref.hidden = true;
        ref.parentNode.insertBefore(cloneElm, ref.nextSibling);
        cloneElm.style.position = 'absolute';
        cloneElm.style.zIndex = 9999;
        cloneElm.style.width = "300px";

        let shiftX = e.clientX - cloneElm.getBoundingClientRect().left;
        let shiftY = e.clientY - cloneElm.getBoundingClientRect().top;

        document.body.append(cloneElm);
        ref.hidden = false;
        ref.style.opacity = 0.4;

        var moveAt = (pageX, pageY) => {
            cloneElm.style.left = pageX - shiftX - 20 + 'px';
            cloneElm.style.top = pageY - shiftY - 20 + 'px';
        }

        let droppableColumnId = null;
        var onMouseMove = (e) => {
            moveAt(e.pageX, e.pageY);

            cloneElm.hidden = true;
            let elm = document.elementFromPoint(e.clientX, e.clientY);
            cloneElm.hidden = false;
            if (!elm) return;

            let isDroppable = elm.classList.contains("droppable");
            if (isDroppable === true) {
                try {
                    droppableColumnId = Number(elm.getAttribute("data-column-id"));
                } catch (ex) {
                    droppableColumnId = null;
                    console.error(ex);
                }
            } else {
                droppableColumnId = null;
            }
        }

        window.addEventListener("mousemove", onMouseMove);
        cloneElm.ondragstart = () => false;
        const _self = this;
        window.onmouseup = (e) => {
            if (droppableColumnId != null) {
                _self.props.moveCardToColumn(droppableColumnId);
            }
            window.removeEventListener('mousemove', onMouseMove);
            window.onmouseup = null;
            cloneElm.outerHTML = "";
            ref.hidden = false;
        }
    }

    render() {
        return (
            <div
                className="card"
                ref={o => { this.ref = o }}
            >
                <div className="card-heading">
                    <div className="card-title">
                        {((this.props.dependencies.get("parentList").size > 0) ? "*" : "") + this.props.title}
                    </div>
                    <div className="card-actions">
                        <button
                            className="btn btn-sm btn-transparent"
                            onMouseDown={this.onClickShowEditCardModal}
                        >
                            <FaEdit />
                        </button>
                        <button className="btn btn-sm btn-transparent" onMouseDown={this.onMouseDown}><FaMove /></button>
                        <button className="btn btn-sm btn-transparent color-red" onMouseDown={this.onClickRemoveCard}><FaRemove /></button>
                    </div>
                </div>
                <div className="card-description">
                    {this.props.description}
                </div>
                <div className="card-labels">
                    {
                        this.props.cardLabels
                            .map(relation => {
                                let label = this.props.labels.find(label => label.get("id") === relation.get(1))
                                return <CardLabel
                                    display="noButtons"
                                    key={label.get("id")}
                                    id={label.get("id")}
                                    name={label.get("name")}
                                    color={relation.get(2)}
                                />
                            })
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    dependencies: state.cardReducer.get("cardDependencies").get(ownProps.id),
    cardLabels: state.cardReducer.get("cardLabelRelation").filter(relation => relation.get(0) === ownProps.id),
    labels: state.cardReducer.get("labels")
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    showEditCardModal: params => dispatch(modalShow("EditCard", params)),
    moveCardToColumn: columnId => dispatch(moveCardToColumn(ownProps.id, columnId)),
    removeCard: () => dispatch(removeCard(ownProps.id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Card);
