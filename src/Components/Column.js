import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeColumn, changeColumnTitle, createCard, removeColumnCards } from '../Actions';
import Card from '../Components/Card';
import { defaults } from '../Configs'
import '../Styles/Column.css';

class Column extends Component {
	inputRef = null;

	constructor(props) {
		super(props);
		this.state = {
			isChangingTitle: false,
			inputTitle: this.props.title
		};

		this.onKeyPressTitleInput = this.onKeyPressTitleInput.bind(this);
		this.onClickTitle = this.onClickTitle.bind(this);
		this.onClickRemoveColumn = this.onClickRemoveColumn.bind(this);
	}

	onKeyPressTitleInput(e) {
		if (e.key === 'Enter') {
			this.props.changeColumnTitle(this.state.inputTitle);
			this.setState({
				isChangingTitle: false
			});
		}
	}

	onClickTitle(e) {
		this.setState({
			isChangingTitle: true,
			inputTitle: this.props.title
		}, () => {
			this.inputRef.focus();
			this.inputRef.select();
		});
	}

	onClickRemoveColumn(e) {
		this.props.removeColumn();
		this.props.removeColumnCards();
	}

	render() {
		return (
			<div className="column droppable" data-column-id={this.props.id}>
				<div className="column-header">
					<div className="column-title">
						{
							(this.state.isChangingTitle === false)
								? <div onClick={this.onClickTitle}>
									{this.props.title + " (" + this.props.cardCount + ")"}
								</div>
								: <input
									ref={o => { this.inputRef = o; }}
									type="text"
									value={this.state.inputTitle}
									onChange={(e) => { this.setState({ inputTitle: e.target.value }) }}
									onKeyPress={this.onKeyPressTitleInput}
									style={{ width: "100px", padding: "2px" }}
								/>
						}
					</div>
					<div className="column-actions">
						<button className="btn btn-orange btn-sm" onClick={this.props.createCard}>Add Card</button>
						<button className="btn btn-default btn-sm" onClick={this.onClickRemoveColumn}>Remove</button>
					</div>
				</div>
				<div className="column-content">
					{
						this.props.cards
							.filter(card => card.get("columnId") === this.props.id)
							.map((card) => {
								return <Card
									key={card.get("id")}
									id={card.get("id")}
									title={card.get("title")}
									description={card.get("description")}
								/>
							})
					}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => ({
	cards: state.cardReducer.get("cards"),
	cardCount: state.cardReducer.get("cards").count(card => card.get("columnId") === ownProps.id)
});

const mapDispatchToProps = (dispatch, ownProps) => ({
	removeColumn: () => { dispatch(removeColumn(ownProps.id)) },
	changeColumnTitle: (newTitle) => { dispatch(changeColumnTitle(ownProps.id, newTitle)) },
	createCard: () => { dispatch(createCard(ownProps.id, defaults.card.title, defaults.card.description)) },
	removeColumnCards: () => { dispatch(removeColumnCards(ownProps.id)) }
});

export default connect(mapStateToProps, mapDispatchToProps)(Column);
