import React, { Component } from 'react';
import '../Styles/CardLabel.css';
import { connect } from 'react-redux';
import { removeLabel } from "../Actions";
import FaRemove from "react-icons/lib/fa/close";

class CardLabel extends Component {

	displayTypes = {
		noButtons: {
			display: "inline-block"
		},
		withButtons: {
			display: "flex",
			alignItems: "center",
			justifyContent: "space-between"
		}
	}

	render() {
		return (
			<div className="cardlabel" style={{ ...this.displayTypes[this.props.display], ...{ backgroundColor: this.props.color } }}>
				<div>
					{this.props.name}
				</div>
				{
					(this.props.display === "withButtons")
						? <div>
							<button className="btn btn-transparent" onClick={this.props.removeLabel}><FaRemove /></button>
						</div>
						: ""
				}
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch, ownProps) => ({
	removeLabel: () => dispatch(removeLabel(ownProps.cardId, ownProps.id))
});

export default connect(null, mapDispatchToProps)(CardLabel);
