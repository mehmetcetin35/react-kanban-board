import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    addLabel,
    updateCard,
    modalHide,
    cardAddDependency
} from "../Actions";
import CardLabel from '../Components/CardLabel';
import '../Styles/EditCard.css';
import FaPlus from 'react-icons/lib/fa/plus';
import FaClose from 'react-icons/lib/fa/close';

class EditCard extends Component {
    labelColors = [
        "#3385ff",
        "#00cc66",
        "#b30086",
        "#ff8533",
        "#9966ff",
        "#ff6666"
    ]

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            cardId: null,
            /* inputs */
            labelInput: "",
            cardTitleInput: "",
            cardDescriptionInput: "",
            cardDependencySelectedOption: "placeholder",
            /* inputs */
            showAddLabelForm: false,
            initialCardData: {
                title: "",
                description: ""
            },
            activeLabelColorIndex: 0
        };

        this.onClickAddLabel = this.onClickAddLabel.bind(this);
        this.onClickUpdateCard = this.onClickUpdateCard.bind(this);
        this.onKeyPressAddLabelInput = this.onKeyPressAddLabelInput.bind(this);
        this.onClickLabelColorSelect = this.onClickLabelColorSelect.bind(this);
        this.onClickHideModal = this.onClickHideModal.bind(this);
        this.onShow = this.onShow.bind(this);
        this.renderCardDependencyOptionList = this.renderCardDependencyOptionList.bind(this);
        this.renderCardDependencyList = this.renderCardDependencyList.bind(this);
        this.onClickAddDependency = this.onClickAddDependency.bind(this);
    }

    onClickAddLabel(e) {
        const { labelInput, activeLabelColorIndex } = this.state;
        if (labelInput !== "") {
            this.props.addLabel(this.state.cardId, this.state.labelInput, this.labelColors[activeLabelColorIndex]);
            this.setState({ labelInput: "" });
        }
    }

    onClickUpdateCard(e) {
        const { cardTitleInput, cardDescriptionInput } = this.state;
        let title = cardTitleInput !== "" ? cardTitleInput : this.state.initialCardData.title;
        let description = cardDescriptionInput !== "" ? cardDescriptionInput : this.state.initialCardData.description;

        this.props.updateCard(this.state.cardId, title, description);

        var _self = this;
        this.setState({
            cardTitleInput: title,
            cardDescriptionInput: description
        }, () => {
            _self.onClickHideModal(null);
        });
    }

    onKeyPressAddLabelInput(e) {
        if (e.key === "Enter") {
            this.onClickAddLabel(null);
        }
    }

    onClickLabelColorSelect(colorIndex, event) {
        this.setState({
            activeLabelColorIndex: colorIndex
        });
    }

    onClickAddDependency(e) {
        let { cardDependencySelectedOption, cardId } = this.state;
        if (cardDependencySelectedOption !== "placeholder") {
            this.props.cardAddDependency(cardId, Number(cardDependencySelectedOption));
            this.setState({
                cardDependencySelectedOption: "placeholder"
            });
        }
    }

    onClickHideModal(e) {
        this.props.modalHide();
        this.setState({
            loaded: false,
            cardId: null
        });
    }

    onShow() {
        let modalParams = this.props.modal.get("params") || {};
        let { cards } = this.props;
        let card = cards.find(card => card.get("id") === modalParams.cardId),
            title = card.get("title"),
            description = card.get("description");

        var _self = this;
        setTimeout(() => {
            _self.setState({
                ...modalParams,
                loaded: true,
                cardTitleInput: title,
                cardDescriptionInput: description,
                initialCardData: {
                    title,
                    description
                }
            });
        }, 10);
    }

    renderCardDependencyOptionList() {
        let currentCard = this.props.cards.find(card => card.get("id") === this.state.cardId);
        return this.props.cards
            .filter(card => card.get("id") !== currentCard.get("id") && card.get("columnId") === currentCard.get("columnId"))
            .map(card => {
                return <option key={card.get("id")} value={card.get("id")}>{card.get("title")}</option>
            });
    }

    renderCardDependencyList() {
        const { cardId } = this.state;
        const { cardDependencies, cards } = this.props;

        let depList = cardDependencies
            .get(cardId)
            .get("parentList");
        let elmArray = [];

        let _it = depList.values();
        for (let i = 0; i < depList.size; i++) {
            let depCardId = _it.next().value;
            let card = cards.find(card => card.get("id") === depCardId);
            elmArray.push(
                <div className="editcard-dep" key={card.get("id")}>{card.get("title") + "#" + card.get("id")}</div>
            )
        }
        return elmArray;
    }

    render() {
        return (
            <div style={{ display: this.props.modal.get("visible") === true ? 'inherit' : 'none' }}>
                <div className="modal editcard">
                    <div className="modal-head">
                        <div> Edit Card </div>
                        <div> <button className="btn btn-transparent" onClick={this.onClickHideModal}><FaClose /></button> </div>
                    </div>
                    {
                        (this.state.loaded === false)
                            ? "Loading..."
                            :
                            <div>
                                <div className="form-input">
                                    <label className="editcard-input-label">Card Title:</label>
                                    <input className="" type="text" value={this.state.cardTitleInput} onChange={e => this.setState({ cardTitleInput: e.target.value })} placeholder="Enter a title" />
                                </div>
                                <div className="form-input">
                                    <label className="editcard-input-label">Description:</label>
                                    <textarea rows="4" className="" type="text" value={this.state.cardDescriptionInput} onChange={e => this.setState({ cardDescriptionInput: e.target.value })} placeholder="Enter a Description" style={{ resize: "none" }}></textarea>
                                </div>
                                <div className="form-input">
                                    <button className="btn d-block btn-blue" onClick={this.onClickUpdateCard}>Update Card</button>
                                </div>
                                <div className="form-input editcard-form-input-seperator">
                                    <label className="editcard-input-label">Labels:</label>
                                    <div className="form-input d-block editcard-addlabel">
                                        <input className="" type="text" value={this.state.labelInput} onChange={e => this.setState({ labelInput: e.target.value })} placeholder="Add new label" onKeyPress={this.onKeyPressAddLabelInput} />
                                        <button className="btn btn-transparent" onClick={this.onClickAddLabel}><FaPlus /></button>
                                    </div>
                                    <div className="editcard-colorpick">
                                        {
                                            this.labelColors.map((color, index) => (
                                                <div
                                                    key={index}
                                                    className={this.state.activeLabelColorIndex === index ? "colorpick-color-active" : "colorpick-color"}
                                                    style={{ backgroundColor: color }}
                                                    onClick={this.onClickLabelColorSelect.bind(this, index)}
                                                >
                                                </div>
                                            ))
                                        }
                                    </div>
                                    <div className="form-input editcard-label-list">
                                        {
                                            this.props.cardLabelRelations
                                                .filter(relation => relation.get(0) === this.state.cardId)
                                                .map(relation => {
                                                    let label = this.props.labels.find(label => label.get("id") === relation.get(1))
                                                    return <CardLabel
                                                        display="withButtons"
                                                        cardId={this.state.cardId}
                                                        key={label.get("id")}
                                                        id={label.get("id")}
                                                        name={label.get("name")}
                                                        color={relation.get(2)}
                                                    />
                                                })
                                        }
                                    </div>
                                </div>
                                <div className="form-input editcard-form-input-seperator">
                                    <label className="editcard-input-label">Dependencies:</label>
                                    <div className="form-input d-block editcard-addlabel">
                                        <select value={this.state.value} defaultValue="placeholder" onChange={(e) => { this.setState({ cardDependencySelectedOption: e.target.value }) }}>
                                            <option value="placeholder" disabled={true}>Select a card</option>
                                            {
                                                this.renderCardDependencyOptionList()
                                            }
                                        </select>
                                        <button className="btn btn-transparent" onClick={this.onClickAddDependency}><FaPlus /></button>
                                    </div>
                                    <div className="form-input editcard-label-list">
                                        {
                                            this.renderCardDependencyList()
                                        }
                                    </div>
                                </div>
                            </div>
                    }
                </div>
                <div className="modal-blur"></div>
                {
                    /* to trigger onShow event for modal */
                    this.state.loaded === false && this.props.modal.get("visible") === true
                        ? this.onShow() || ""
                        : ""
                }
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    cards: state.cardReducer.get("cards"),
    cardLabelRelations: state.cardReducer.get("cardLabelRelation"),
    cardDependencies: state.cardReducer.get("cardDependencies"),
    labels: state.cardReducer.get("labels"),
    modal: state.modalReducer.get("EditCard")
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    addLabel: (cardId, labelName, color) => dispatch(addLabel(cardId, labelName, color)),
    updateCard: (cardId, title, description) => dispatch(updateCard(cardId, title, description)),
    modalHide: () => dispatch(modalHide("EditCard")),
    cardAddDependency: (cardId, dependencyId) => dispatch(cardAddDependency(cardId, dependencyId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditCard);