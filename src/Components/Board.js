import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createColumn, modalShow, changeBoardName } from '../Actions';
import { defaults } from '../Configs/';
import Column from '../Components/Column';
import EditCard from '../Components/EditCard';
import '../Styles/Board.css';
import boardIcon from '../Images/list.png';
import FaPlus from 'react-icons/lib/fa/plus';
import FaUpload from 'react-icons/lib/fa/upload';

class Board extends Component {
    inputRef = null
    constructor(props) {
        super(props)
        this.state = {
            isChangingName: false,
            inputName: props.name
        }
        this.onClickAddNewColumn = this.onClickAddNewColumn.bind(this);
        this.onClickExportAsJSON = this.onClickExportAsJSON.bind(this);
        this.onClickName = this.onClickName.bind(this);
        this.onKeyPressNameInput = this.onKeyPressNameInput.bind(this);
    }

    onClickAddNewColumn() {
        this.props.createColumn(defaults.column.title);
    }

    onClickExportAsJSON(e) {
        this.props.jsonHandlerModalShow({ modalType: "saveData" });
    }

    onClickName(e) {
        this.setState({
            isChangingName: true,
            inputName: this.props.name
        }, () => {
            this.inputRef.focus();
            this.inputRef.select();
        });
    }

    onKeyPressNameInput(e) {
        if (e.key === 'Enter') {
            this.props.changeBoardName(this.state.inputName);
            this.setState({
                isChangingName: false
            });
        }
    }

    render() {
        return (
            <div className="board">
                <div className="board-head">
                    <div className="board-title-container">
                        <div className="board-icon">
                            <img src={boardIcon} alt="board-icon" />
                        </div>
                        <div className="board-name margin-lr">
                            {
                                (this.state.isChangingName === false)
                                    ? <div onClick={this.onClickName}>
                                        {this.props.name}
                                    </div>
                                    : <input
                                        ref={o => { this.inputRef = o; }}
                                        type="text"
                                        value={this.state.inputName}
                                        onChange={(e) => { this.setState({ inputName: e.target.value }) }}
                                        onKeyPress={this.onKeyPressNameInput}
                                        style={{ width: "150px", padding: "4px" }}
                                    />
                            }
                        </div>
                    </div>
                    <div className="board-actions margin-lr">
                        <button className="btn btn-orange" onClick={this.onClickAddNewColumn}>{<FaPlus />} Add New List</button>
                        <button className="btn btn-blue" onClick={this.onClickExportAsJSON}>{<FaUpload />} Export as JSON</button>
                    </div>
                </div>
                <div className="board-content">
                    {
                        this.props.columns.map((column) => {
                            return <Column
                                key={column.get("id")}
                                id={column.get("id")}
                                title={column.get("title")}
                            />
                        })
                    }
                </div>
                <EditCard />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    columns: state.columnReducer
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    createColumn: title => dispatch(createColumn(title, ownProps.id)),
    jsonHandlerModalShow: params => dispatch(modalShow("JsonHandler", params)),
    changeBoardName: name => dispatch(changeBoardName(ownProps.id, name))
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
