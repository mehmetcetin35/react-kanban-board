import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    modalHide,
    loadJSONBoard,
    loadJSONColumn,
    loadJSONCards
} from "../Actions";
import '../Styles/JsonHandler.css';
import FaClose from 'react-icons/lib/fa/close';

class JsonHandler extends Component {
    jsonContainerInputRef = null
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            JSONInput: ""
        };

        this.onClickHideModal = this.onClickHideModal.bind(this);
        this.onShow = this.onShow.bind(this);
        this.onClickLoadFromJSON = this.onClickLoadFromJSON.bind(this);
        this.saveAsJSON = this.saveAsJSON.bind(this);
        this.onClickCopyContent = this.onClickCopyContent.bind(this);
    }

    onClickLoadFromJSON(e) {
        try {
            const { JSONInput } = this.state;
            const input = JSON.parse(JSONInput);

            this.props.loadJSONBoard(input.boardList);
            this.props.loadJSONColumn(input.columnList);
            this.props.loadJSONCards(input.cardList);

            this.onClickHideModal(null);

        } catch (ex) {
            console.error(ex);
            alert("Invalid JSON!");
        }
    }

    saveAsJSON(e) {
        const {
            boardData,
            columnData,
            cardData
        } = this.props;

        let output = {
            boardList: boardData.toJS(),
            columnList: columnData.toJS(),
            cardList: cardData.toJS()
        };

        this.setState({
            JSONInput: JSON.stringify(output, null, 2)
        });
    }

    onClickCopyContent(e) {
        let { jsonContainerInputRef } = this;
        if (jsonContainerInputRef.select === undefined) {
            //pre
            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents(jsonContainerInputRef);
            selection.removeAllRanges();
            selection.addRange(range);
        } else {
            //textarea
            jsonContainerInputRef.select();
        }
        document.execCommand("Copy");
    }

    onClickHideModal(e) {
        this.props.modalHide();
        this.setState({
            loaded: false
        });
    }

    onShow() {
        let modalParams = this.props.modal.get("params") || {};
        var _self = this;
        setTimeout(() => {
            _self.setState({
                ...modalParams,
                loaded: true,
                JSONInput: ""
            }, () => {
                if (this.state.modalType === "saveData") {
                    this.saveAsJSON();
                }
            });
        }, 10);
    }

    render() {
        return (
            <div style={{ display: this.props.modal.get("visible") === true ? 'inherit' : 'none' }}>
                <div className="modal">
                    <div className="modal-head">
                        <div> Json Data </div>
                        <div> <button className="btn btn-transparent" onClick={this.onClickHideModal}><FaClose /></button> </div>
                    </div>
                    <div className="modal-body">
                        <div className="form-input" style={{ textAlign: "right" }}>
                            <button className="btn btn-default" onClick={this.onClickCopyContent}>Copy Content</button>
                        </div>
                        <div>
                            {
                                (this.state.modalType && this.state.modalType === "loadData")
                                    ? <textarea
                                        ref={o => { this.jsonContainerInputRef = o }}
                                        className="d-block"
                                        rows="20"
                                        style={{ resize: "none", padding: "5px" }}
                                        value={this.state.JSONInput}
                                        onChange={(e) => { this.setState({ JSONInput: e.target.value }) }}
                                    ></textarea>
                                    : <pre
                                        ref={o => { this.jsonContainerInputRef = o }}
                                        style={{ maxHeight: "400px", overflowY: "scroll" }}
                                    >
                                        {this.state.JSONInput}
                                    </pre>
                            }
                        </div>
                    </div>
                    <div className="modal-footer">
                        {
                            (this.state.modalType && this.state.modalType === "loadData")
                                ? <button className="btn btn-green" onClick={this.onClickLoadFromJSON}>Load Board</button>
                                : ""
                        }

                    </div>
                </div>
                <div className="modal-blur"></div>
                {
                    /* to trigger onShow event for modal */
                    this.state.loaded === false && this.props.modal.get("visible") === true
                        ? this.onShow() || ""
                        : ""
                }
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    modal: state.modalReducer.get("JsonHandler"),
    boardData: state.boardReducer,
    columnData: state.columnReducer,
    cardData: state.cardReducer
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    modalHide: () => dispatch(modalHide("JsonHandler")),
    loadJSONBoard: (json) => dispatch(loadJSONBoard(json)),
    loadJSONColumn: (json) => dispatch(loadJSONColumn(json)),
    loadJSONCards: (json) => dispatch(loadJSONCards(json))
});

export default connect(mapStateToProps, mapDispatchToProps)(JsonHandler);