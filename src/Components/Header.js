import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  createBoard,
  modalShow,
  loadJSONBoard,
  loadJSONColumn,
  loadJSONCards
} from '../Actions';
import { initialBoard } from '../Configs';
import '../Styles/Header.css';
import FaPlus from 'react-icons/lib/fa/plus';

class Header extends Component {
  constructor(props) {
    super(props);
    this.onClickCreateBoard = this.onClickCreateBoard.bind(this);
    this.onClickLoadFromJSON = this.onClickLoadFromJSON.bind(this);
  }

  onClickCreateBoard(e) {
    e.preventDefault();

    const { boards } = this.props;
    if (boards.size > 0) {
      return console.log("Cannot create more than one boards.");
    }

    try {
      this.props.loadJSONBoard(initialBoard.boardList);
      this.props.loadJSONColumn(initialBoard.columnList);
      this.props.loadJSONCards(initialBoard.cardList);
    } catch (ex) {
      console.error(ex);
    }
  }

  onClickLoadFromJSON(e) {
    this.props.jsonHandlerModalShow({ modalType: "loadData" });
  }

  render() {
    return (
      <header className="header">
        <div style={{ width: "25%" }}></div>
        <div className="header-title">
          Kanban-Board
            </div>
        <div className="header-actions">
          <button className="btn btn-orange" onClick={this.onClickCreateBoard}>{<FaPlus />} Create Board</button>
          <button className="btn btn-green" onClick={this.onClickLoadFromJSON}>{<FaPlus />} Load from JSON</button>
        </div>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  boards: state.boardReducer
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  createBoard: name => dispatch(createBoard(name)),
  jsonHandlerModalShow: (params) => dispatch(modalShow("JsonHandler", params)),
  loadJSONBoard: (json) => dispatch(loadJSONBoard(json)),
  loadJSONColumn: (json) => dispatch(loadJSONColumn(json)),
  loadJSONCards: (json) => dispatch(loadJSONCards(json))
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
