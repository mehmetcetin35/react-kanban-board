/* Board */
export const createBoard = (name) => ({
    type: "BOARD_CREATE",
    name
});

export const loadJSONBoard = (boards) => ({
    type: "BOARD_LOAD_JSON",
    boards
});

export const changeBoardName = (id, name) => ({
    type: "BOARD_CHANGE_NAME",
    id,
    name
});

/* Modal */
export const modalShow = (modalName, params = null) => ({
    type: "MODAL_SHOW",
    modalName,
    params
});

export const modalHide = (modalName, params = null) => ({
    type: "MODAL_HIDE",
    modalName
});

/* Column */
export const createColumn = (title, boardId) => ({
    type: "COLUMN_CREATE",
    boardId,
    title
});

export const loadJSONColumn = (columns) => ({
    type: "COLUMN_LOAD_JSON",
    columns
});

export const removeColumn = (id) => ({
    type: "COLUMN_REMOVE",
    id
});

export const changeColumnTitle = (id, title) => ({
    type: "COLUMN_CHANGE_TITLE",
    id,
    title
});

/* Card */
export const createCard = (columnId, title, description) => ({
    type: "CARD_CREATE",
    columnId,
    title,
    description
});

export const loadJSONCards = (cardData) => ({
    type: "CARDS_LOAD_JSON",
    cardData
});

export const removeCard = (id) => ({
    type: "CARD_REMOVE",
    id
});

export const removeColumnCards = (columnId) => ({
    type: "CARD_REMOVE_COLUMN",
    columnId
});

export const addLabel = (cardId, labelName, color) => ({
    type: "CARD_ADD_LABEL",
    cardId,
    labelName,
    color
});

export const removeLabel = (cardId, labelId) => ({
    type: "CARD_REMOVE_LABEL",
    cardId,
    labelId
});

export const updateCard = (id, title, description) => ({
    type: "CARD_UPDATE",
    id,
    title,
    description
});

export const moveCardToColumn = (cardId, columnId) => ({
    type: "CARD_CHANGE_COLUMN",
    cardId,
    columnId
});

export const cardAddDependency = (cardId, dependencyId) => ({
    type: "CARD_ADD_DEPENDENCY",
    cardId,
    dependencyId
});
