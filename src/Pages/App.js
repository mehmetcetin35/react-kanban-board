import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../Styles/App.css';
import Header from '../Components/Header';
import Board from '../Components/Board';
import JsonHandler from '../Components/JsonHandler';

class App extends Component {
  render() {
    const { boards } = this.props;
    return (
      <div className="App">
        <JsonHandler />
        <Header />
        {
          (this.props.boards.size > 0)
            ? (<Board
              id={boards.get(0).get("id")}
              name={boards.get(0).get("name")}
            />)
            : <div className="App-empty-content">Click "Create Board" or load board from JSON data to start.</div>}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  boards: state.boardReducer
});

export default connect(mapStateToProps, null)(App);
