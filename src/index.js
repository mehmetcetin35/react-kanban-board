import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Pages/App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
//reducers
import boardReducer from './Reducers/board';
import columnReducer from './Reducers/column';
import cardReducer from './Reducers/card';
import modalReducer from './Reducers/modal';

const store = createStore(combineReducers({
    boardReducer,
    columnReducer,
    cardReducer,
    modalReducer
}));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
