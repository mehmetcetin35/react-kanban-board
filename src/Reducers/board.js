import { Map, List } from "immutable";

let _boards = List(),
    _lastId = 0;

const boardReducer = (state = _boards, action) => {
    switch (action.type) {
        case "BOARD_CREATE":
            let board = Map({
                id: _lastId++,
                name: action.name
            });
            _boards = _boards.push(board);
            return _boards;

        case "BOARD_LOAD_JSON":
            const { boards } = action;
            let maxId = 0;
            _boards = List();
            for (let i = 0; i < boards.length; i++) {
                let board = Map({
                    id: boards[i].id,
                    name: boards[i].name
                });
                maxId = (boards[i].id > maxId) ? boards[i].id : maxId;
                _boards = _boards.push(board);
            }
            _lastId = maxId + 1;
            return _boards;

        case "BOARD_CHANGE_NAME":
            const { id, name } = action;
            let boardFoundIndex = _boards.findIndex(board => board.get("id") === id);
            if (boardFoundIndex === -1) {
                console.error("Cannot find board with defined id.");
                return _boards;
            }
            _boards = _boards.update(boardFoundIndex, board => {
                board = board.set("name", name);
                return board;
            });
            return _boards;

        default:
            return state;
    }
}

export default boardReducer;