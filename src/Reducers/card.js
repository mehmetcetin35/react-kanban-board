import { Map, List, Set } from "immutable";

let _cardState = Map({
    cards: List(),
    labels: List(),
    cardLabelRelation: List(), /* cardId, labelId, labelColor */
    cardDependencies: Map() /* dependency tree */
}),
    _lastCardId = 0,
    _lastLabelId = 0;

const cardReducer = (state = _cardState, action) => {
    let cards = _cardState.get("cards"),
        labels = _cardState.get("labels"),
        cardLabelRelation = _cardState.get("cardLabelRelation"),
        cardDependencies = _cardState.get("cardDependencies");

    switch (action.type) {
        case "CARD_CREATE":
            let newCard = Map({
                id: _lastCardId++,
                columnId: action.columnId,
                title: action.title,
                description: action.description
            });
            cards = cards.push(newCard);

            let dependencyRelation = Map({
                cardId: newCard.get("id"),
                parentList: Set(),
                childrenList: Set()
            });
            cardDependencies = cardDependencies.set(newCard.get("id"), dependencyRelation);

            let cardLabelIds = List();
            for (let i = 0; i < action.labels; i++) {
                const name = action.labels[i];
                let labelFoundIndex = labels.findIndex(label => label.get("name") === name);
                if (labelFoundIndex === -1) {
                    let newLabel = Map({
                        id: _lastLabelId++,
                        name
                    });
                    labels = labels.push(newLabel);
                    cardLabelIds = cardLabelIds.push(newLabel.get("id"));
                } else {
                    cardLabelIds = cardLabelIds.push(labelFoundIndex);
                }
            }

            for (let i = 0; i < cardLabelIds.size; i++) {
                let newRelation = List([newCard.get("id"), cardLabelIds.get(i)]);
                cardLabelRelation = cardLabelRelation.push(newRelation);
            }

            _cardState = _cardState.set("cardDependencies", cardDependencies);
            _cardState = _cardState.set("cards", cards);
            _cardState = _cardState.set("labels", labels);
            _cardState = _cardState.set("cardLabelRelation", cardLabelRelation);
            return _cardState;

        case "CARDS_LOAD_JSON":
            const { cardData } = action;
            let jsonCards = List(),
                jsonLabels = List(),
                jsonCardLabelRelation = List(),
                jsonCardDependencies = Map(),
                maxCardId = 0,
                maxLabelId = 0;

            for (let i = 0; i < cardData.cards.length; i++) {
                let card = cardData.cards[i];
                let newCard = Map({
                    id: card.id,
                    columnId: card.columnId,
                    title: card.title,
                    description: card.description
                });
                maxCardId = (card.id > maxCardId) ? card.id : maxCardId;
                jsonCards = jsonCards.push(newCard);
            }

            for (let i = 0; i < cardData.labels.length; i++) {
                let label = cardData.labels[i];
                let newLabel = Map({
                    id: label.id,
                    name: label.name
                });
                maxLabelId = (label.id > maxLabelId) ? label.id : maxLabelId;
                jsonLabels = jsonLabels.push(newLabel);
            }

            for (let i = 0; i < cardData.cardLabelRelation.length; i++) {
                let relation = cardData.cardLabelRelation[i];
                let newRelation = List(relation);
                jsonCardLabelRelation = jsonCardLabelRelation.push(newRelation);
            }

            for (let key in cardData.cardDependencies) {
                let dependency = cardData.cardDependencies[key];
                let parentList = Set(dependency.parentList);
                let childrenList = Set(dependency.childrenList);
                let newDependency = Map({
                    cardId: dependency.cardId,
                    parentList,
                    childrenList
                });
                jsonCardDependencies = jsonCardDependencies.set(dependency.cardId, newDependency);
            }

            _cardState = _cardState.set("cards", jsonCards);
            _cardState = _cardState.set("labels", jsonLabels);
            _cardState = _cardState.set("cardLabelRelation", jsonCardLabelRelation);
            _cardState = _cardState.set("cardDependencies", jsonCardDependencies);

            _lastCardId = maxCardId + 1;
            _lastLabelId = maxLabelId + 1;

            return _cardState;

        case "CARD_REMOVE":
            //check if has any child dependencies
            let depList = cardDependencies.get(action.id),
                depChildrenList = depList.get("childrenList");
            if (depChildrenList.size > 0) {
                alert("The card has dependencies, cannot be removed.");
                return _cardState;
            }

            let removeId = cards.findIndex(col => col.get("id") === action.id);
            if (removeId === -1) {
                console.error("Cannot find card with given id");
                return state;
            }
            cards = cards.delete(removeId);

            //remove deps
            let depParentList = depList.get("parentList");
            //remove the card from parent's children dependency lists
            let _it = depParentList.values();
            let parentDepCardId = _it.next().value;
            while (parentDepCardId !== undefined) {
                let parentDep = cardDependencies.get(parentDepCardId);
                parentDep = parentDep.set("childrenList", parentDep.get("childrenList").delete(action.id));
                cardDependencies = cardDependencies.set(parentDepCardId, parentDep);
                parentDepCardId = _it.next().value;
            }

            cardDependencies = cardDependencies.delete(action.id);

            //remove label relations
            cardLabelRelation = cardLabelRelation.filter(relation => relation.get(0) !== action.id);

            _cardState = _cardState.set("cardDependencies", cardDependencies);
            _cardState = _cardState.set("cardLabelRelation", cardLabelRelation);
            _cardState = _cardState.set("cards", cards);

            return _cardState;

        case "CARD_REMOVE_COLUMN":
            let removeCardList = cards.filter(card => card.get("columnId") === action.columnId);

            for (let i = 0; i < removeCardList.size; i++) {
                let cardId = removeCardList.get(i).get("id");
                cardDependencies = cardDependencies.delete(cardId);
                let labelRelationIndexList = cardLabelRelation.filter(relation => relation.get(0) === cardId);
                for (let j = 0; j < labelRelationIndexList.size; j++) {
                    cardLabelRelation = cardLabelRelation.delete(labelRelationIndexList.get(j));
                }
            }

            cards = cards.filter(card => card.get("columnId") !== action.columnId);
            _cardState = _cardState.set("cards", cards);
            _cardState = _cardState.set("cardDependencies", cardDependencies);
            _cardState = _cardState.set("cardLabelRelation", cardLabelRelation);
            return _cardState;

        case "CARD_ADD_LABEL":
            const { cardId, labelName, color } = action;
            let cardIndex = cards.findIndex(card => card.get("id") === cardId);
            if (cardIndex === -1) {
                console.error("Cannot find card with defined id.");
                return _cardState;
            }
            let labelFoundIndex = labels.findIndex(label => label.get("name") === labelName);
            if (labelFoundIndex === -1) {
                //add new label
                let newLabel = Map({
                    id: _lastLabelId++,
                    name: labelName
                });
                labels = labels.push(newLabel);
                cardLabelRelation = cardLabelRelation.push(List([cardIndex, newLabel.get("id"), color]));
            } else {
                let foundRelationIndex = cardLabelRelation.findIndex(relation => relation.get(0) === cardId && relation.get(1) === labelFoundIndex);
                if (foundRelationIndex === -1) {
                    //add found label index to relation list
                    cardLabelRelation = cardLabelRelation.push(List([cardIndex, labelFoundIndex, color]));
                } else {
                    return _cardState;
                }
            }
            _cardState = _cardState.set("cards", cards);
            _cardState = _cardState.set("labels", labels);
            _cardState = _cardState.set("cardLabelRelation", cardLabelRelation);
            return _cardState;

        case "CARD_REMOVE_LABEL": {
            const { cardId, labelId } = action;
            let relationIndex = cardLabelRelation.findIndex(relation => {
                return relation.get(0) === cardId && relation.get(1) === labelId;
            });
            if (relationIndex === -1) {
                console.error("Cannot find card-label relation with defined id.");
                return _cardState;
            }
            cardLabelRelation = cardLabelRelation.delete(relationIndex);
            //if no more card relationships for label, remove it
            let hasRelation = cardLabelRelation.some(relation => relation.get(1) === labelId);
            if (hasRelation === false) {
                let labelIndex = labels.findIndex(label => label.get("id") === labelId);
                if (labelIndex !== -1) {
                    labels = labels.delete(labelIndex);
                }
            }
            _cardState = _cardState.set("labels", labels);
            _cardState = _cardState.set("cardLabelRelation", cardLabelRelation);
            return _cardState;
        }

        case "CARD_UPDATE":
            let cardFoundIndex = cards.findIndex(card => card.get("id") === action.id);
            if (cardFoundIndex === -1) {
                console.error("Cannot find card with defined id.");
                return _cardState;
            }
            cards = cards.update(cardFoundIndex, card => {
                card = card.set("title", action.title);
                card = card.set("description", action.description);
                return card;
            });
            _cardState = _cardState.set("cards", cards);
            return _cardState;

        case "CARD_CHANGE_COLUMN": {
            let cardFoundIndex = cards.findIndex(card => card.get("id") === action.cardId);
            if (cardFoundIndex === -1) {
                console.error("Cannot find card with defined id.");
                return _cardState;
            }
            cards = cards.update(cardFoundIndex, card => {
                card = card.set("columnId", action.columnId);
                return card;
            });
            _cardState = _cardState.set("cards", cards);
            return _cardState
        }

        case "CARD_ADD_DEPENDENCY": {
            let childDepId = action.cardId,
                parentDepId = action.dependencyId;

            let childDep = cardDependencies.get(childDepId);
            let parentDep = cardDependencies.get(parentDepId);

            //check if wanted to add in reversed roles
            if (parentDep.get("parentList").includes(childDepId)) {
                alert("The card has same dependency as parent!");
                return _cardState;
            }

            childDep = childDep.set("parentList", childDep.get("parentList").add(parentDepId));
            parentDep = parentDep.set("childrenList", childDep.get("childrenList").add(childDepId));

            cardDependencies = cardDependencies.set(childDepId, childDep);
            cardDependencies = cardDependencies.set(parentDepId, parentDep);
            _cardState = _cardState.set("cardDependencies", cardDependencies);
            return _cardState
        }

        default:
            return state;
    }
}

export default cardReducer;