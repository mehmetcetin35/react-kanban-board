import { Map, List } from "immutable";

let _columns = List(),
    _lastId = 0;

const columnReducer = (state = _columns, action) => {
    switch (action.type) {
        case "COLUMN_CREATE":
            let newColumn = Map({
                id: _lastId++,
                boardId: action.boardId,
                title: action.title
            });
            _columns = _columns.push(newColumn);
            return _columns;

        case "COLUMN_LOAD_JSON":
            const { columns } = action;
            _columns = List();
            let maxId = 0;
            for (let i = 0; i < columns.length; i++) {
                let newColumn = Map({
                    id: columns[i].id,
                    boardId: columns[i].boardId,
                    title: columns[i].title
                });
                maxId = (columns[i].id > maxId) ? columns[i].id : maxId;
                _columns = _columns.push(newColumn);
            }
            _lastId = maxId + 1;
            return _columns;

        case "COLUMN_REMOVE":
            let removeId = _columns.findIndex(col => col.get("id") === action.id);
            if (removeId === -1) {
                console.error("Cannot find column with given id");
                return state;
            }
            _columns = _columns.delete(removeId);
            return _columns;

        case "COLUMN_CHANGE_TITLE":
            let changeId = _columns.findIndex(col => col.get("id") === action.id);
            if (changeId === -1) {
                console.error("Cannot find column with given id");
                return state;
            }
            _columns = _columns.update(changeId, column => column.set("title", action.title));
            return _columns;

        default:
            return state;
    }
}

export default columnReducer;