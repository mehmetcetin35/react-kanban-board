import { Map } from "immutable";

let _modalState = Map({
    "EditCard": Map({
        visible: false,
        params: null
    }),
    "JsonHandler": Map({
        visible: false,
        params: null
    })
});

const modalReducer = (state = _modalState, action) => {
    let modal = _modalState.get(action.modalName);
    switch (action.type) {
        case "MODAL_SHOW":
            modal = modal.set("visible", true);
            modal = modal.set("params", action.params || null);
            _modalState = _modalState.set(action.modalName, modal);
            return _modalState;

        case "MODAL_HIDE":
            modal = modal.set("visible", false);
            _modalState = _modalState.set(action.modalName, modal);
            return _modalState;

        default:
            return state;
    }
};

export default modalReducer;